const Board = require('../models/board');

class BoardsController {
    static async getBoards(req, res) {
        try {
            const { id } = req.body;
            const boards = await Board.find({ userId: id }).sort({ createdAt: -1 });
            if (!boards.length) {
                return res.status(404).send({ status: false, message: 'Boards not found' });
            }
            return res.send({ status: true, data: boards })
        } catch (e) {
            return res.status(500).send({ status: false, message: e.message });

        }
    }

    static async postBoards(req, res) {
        try {
            const { id } = req.body;
            const board = await new Board({
                name: req.body.name,
                description: req.body.description,
                userId: id
            });
            await board.save();
            res.send({ status: true, board });
        } catch (e) {
            res.status(404).send({ status: false, error: e.message });
        }
    }

    static async deleteBoards(req, res) {
        try {
            const { id } = req.body;
            const deleted = await Board.findByIdAndDelete(id);
            if (!deleted) {
                return res.status(404).send({ status: false, message: 'No such board or it is already deleted' })
            }
            res.send({ status: true, board: deleted });
        } catch (e) {
            res.status(404).send({ message: e.message });
        }
    }

    static async patchBoards(req, res) {
        try {
            const { id, updateBy, value } = req.body;
            const updateField = updateBy === 'name' ? { name: value } : { description: value };
            await Board.findByIdAndUpdate(id, updateField);
            res.send({ status: true, board: await Board.findById(id) });
        } catch (e) {
            res.send(400).send({ status: false, message: e.message });
        }
    }
}

module.exports = BoardsController;

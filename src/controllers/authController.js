require('dotenv').config();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const SECRET_KEY = process.env.SECRET_KEY;

class AuthController {
    static async postRegister(req, res) {
        try {
            const salt = await bcrypt.genSalt(10);
            const hashedPassword = await bcrypt.hash(req.body.password, salt);
            const existUser = await User.findOne({ email: req.body.email });
            if (!!existUser) {
                return res.status(404).send({ message: 'User with such email already exist' });
            }
            const user = new User({
                name: req.body.name,
                email: req.body.email,
                password: hashedPassword
            })
            const result = await user.save();
            const { password, ...data } = await result.toJSON();
            res.send(data);
        } catch (e) {
            return res.status(404).send({ message: 'User with such email already exist' });
        }
    }

    static async postLogin(req, res) {
        try {
            const user = await User.findOne({ email: req.body.email });
            if (!user) {
                return res.status(404).send({ message: 'User not found' });
            }
            if (!await bcrypt.compare(req.body.password, user.password)) {
                return res.status(404).send({ message: 'Incorrect password' });
            }
            const token = jwt.sign({ _id: user._id }, SECRET_KEY);
            const day = 24 * 60 * 60 * 1000;
            res.cookie('jwt', token, { httpOnly: true, maxAge: day });
            const { password, ...data } = await user.toJSON();
            res.send({ status: true, data });
        } catch (e) {
            res.status(500).send({ status: false, message: e.message });
        }
    }

    static async getAuth(req, res) {
        try {
            const cookie = req.cookies['jwt'];
            const claims = jwt.verify(cookie, SECRET_KEY);
            if (!claims) {
                return res.status(401).send({ message: 'Unauthenticated' });
            }
            const user = await User.findOne({ _id: claims._id });
            const { password, ...data } = await user.toJSON();
            res.send(data);
        } catch {
            return res.status(401).send({ message: 'Unauthenticated' });
        }
    }

    static postLogout(req, res) {
        try {
            res.cookie('jwt', '', { maxAge: 0 });
            res.send({ message: 'Success' });
        } catch (e) {
            res.status(500).send({ message: e.message });
        }
    }
}

module.exports = AuthController;

require('dotenv').config();
const jwt = require('jsonwebtoken');
const SECRET_KEY = process.env.SECRET_KEY;

module.exports = (req, res, next) => {
    try {
        const cookie = req.cookies['jwt'];
        if (!cookie) {
            return res.status(401).send({ status: false, message: 'Unauthenticated' });
        }
        jwt.verify(cookie, SECRET_KEY);
        next();
    } catch (e) {
        return res.status(401).send({ message: 'Unauthenticated', error: e.message, status: false });
    }
}

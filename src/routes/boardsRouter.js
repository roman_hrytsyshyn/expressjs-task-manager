const boardsRouter = require('express').Router();
const isAuth = require('../middlewares/isAuth');
const BoardsController = require('../controllers/boardsController');

boardsRouter.get('/', isAuth, BoardsController.getBoards);

boardsRouter.post('/', isAuth, BoardsController.postBoards);

boardsRouter.delete('/', isAuth, BoardsController.deleteBoards);

boardsRouter.patch('/', isAuth, BoardsController.patchBoards);

module.exports = boardsRouter;

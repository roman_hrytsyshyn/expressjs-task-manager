const authRouter = require('express').Router();
const AuthController = require('../controllers/authController');

authRouter.post('/register', AuthController.postRegister);

authRouter.post('/login', AuthController.postLogin);

authRouter.get('/auth', AuthController.getAuth);

authRouter.post('/logout', AuthController.postLogout);

module.exports = authRouter;

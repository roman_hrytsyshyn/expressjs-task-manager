require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const cookieParser = require('cookie-parser');

const logger = require('./src/middlewares/logger');
const authRouter = require('./src/routes/authRouter');

const PORT = process.env.PORT || 5000;
const DB_PASS = process.env.DB_PASSWORD;
const app = express();

const boardsRouter = require('./src/routes/boardsRouter');

mongoose.connect(
    `mongodb+srv://admin:${DB_PASS}@task-manager-express.i7yj8mb.mongodb.net/task-manager?retryWrites=true&w=majority`,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => app.listen(PORT, () => console.log(`Server started on port ${PORT}...`))
);

app.use(express.json());
app.use(cookieParser());
app.use(cors({ credentials: true, origin: [ 'http://localhost:4200' ] }));

app.use(logger);

app.use('/api/boards', boardsRouter);
app.use('/api', authRouter);


/**
 * GET /api/boards ---> в req.body передаємо id користувача і по ньому повертаємо массив усіх boards користувача
 * POST /api/boards ---> в req.body передаємо name, description i id користувача, повертаємо новий массив усіх boards користувача
 * */
